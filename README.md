# **INDEPENDANCE-2DAY**

Gestion d'un foyer

## **Mise en place projet**
### 1. **BDD**

mettre à jour le fichier .env
Activé mysql (avec xampp pour windows) 

1.1 **pour créer la bdd** 
> symfony console d:d:c (php bin/console sans le cli symfony)

1.2 **pour update le schémas de la bdd**
> symfony console d:s:u -f (php bin/console sans le cli de symfony)

### 2. **Server Web**

2.1.  
**avec le cli de symfony**
> symfony server:start

**Sans cli** 
> php -S localhost:8000 -t public

## 3. **Developpement** 
### 3.1 **Front** 

**SASS**
> sass --watch public/assets/scss/style.scss:public/assets/src/css/style.css

3.2 **Rajouter de la trad**

> php bin/console translation:update --force fr => pour le francais 

> php bin/console translation:update --force en => pour l'anglais 

3.3.  **Datafixtures**
> symfony console d:f:l

## **ANNEXE**
**Probléme Git**

En changeant le mdp de git, l'authentification. 

**Cette commande vient réinitialiser l'authentification auto de git.**
> git credential-manager remove -force

**Clear le cache**
> php bin/console cache:clear


testetststststs