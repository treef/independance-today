<?php


namespace App\Controller\Member\RDV;

use App\Entity\rdv\Rdv;
use App\Repository\RDVRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use OpenFoodFacts;


/**
* @Route("/member")
*/
class DetailRdvController extends AbstractController
{
    /** @var RDVRepository */
    private $rdvRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(Rdv::class);
       $this->rdvRepository = $manager->getRepository(Rdv::class);
   }


    /**
     * @Route("/rdv/{rdv}", name="detail_rdv")
     */
    public function _invoke(Rdv $rdv)
    {
        /*$api = new \OpenFoodFacts\Api('food','fr');
        //https://github.com/openfoodfacts/openfoodfacts-php/blob/develop/src/Api.php
        //$product = $api->search('nutella'); recherche par rapport à un mot (cle ?)
        $product = $api->getProduct('3057640385148');
        //Récupérer les données dans un array
        $products = $api->search('nutella');
        echo "<pre>";
        //var_dump($product->getData());
        //var_dump($products->getData());
        echo "</pre>";
        foreach($products as $product)
        {
            var_dump($product->getData()["code"]);
        }
        //Récupérer une données selon le nom de l'attribut
        //var_dump($product->__get('correctors_tags'));

        die;
        */
        return $this->render('member/RDV/detailRDV.html.twig',
            [
                'mainNavHome'=>true,
                'rdv' => $rdv,
                'title' => "Détail du RDV " . $rdv->getId()
            ]
        );
        
    }
}