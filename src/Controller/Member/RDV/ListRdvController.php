<?php


namespace App\Controller\Member\RDV;

use App\Entity\listeCadeau\Cadeau;
use App\Entity\listeCadeau\Evenement;
use App\Entity\listeCadeau\Member;
use App\Entity\rdv\Rdv;
use App\Repository\RDVRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class ListRdvController extends AbstractController
{
    /** @var RDVRepository */
    private $rdvRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(Rdv::class);
       $this->rdvRepository = $manager->getRepository(Rdv::class);
   }


    /**
     * @Route("/mes_rendezvous", name="rdvs")
     */
    public function _invoke()
    {
        $rdvs = $this->rdvRepository->findAllRdvDisponible($this->getUser()->getId());
        
        return $this->render('member/RDV/listRdv.html.twig',
            [
                'mainNavHome'=>true,
                'title' => "Liste des RDV",
                'rdvs' => $rdvs
            ]
        );

    }
}