<?php


namespace App\Controller\Member;

use App\Form\AddProductForm;
use App\Form\CategoryForm;
use App\Form\UserType;
use App\Repository\CategoryRepository;
use App\Repository\DocumentRepository;
use App\Repository\LibraryRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
* @Route("/member")
*/
class MonProfiController extends AbstractController
{

   public function __construct(ManagerRegistry $registry)
   {
   }


    /**
     * @Route("/mon-profil/", name="mon-profil")
     */
    public function _invoke(Request $request):Response
    {
        $form = $this->createForm(UserType::class, $this->getUser());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('mon-profil');
        }

        return $this->render('Member/monProfil.html.twig', 
            [
            'form' => $form->createView(),
            'mainNavRegistration' => true,
            "user" => $this->getUser(),
            'title' => 'Mon Profil',
            ]
        );

    }
}