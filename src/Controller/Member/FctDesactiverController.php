<?php


namespace App\Controller\Member;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class FctDesactiverController extends AbstractController
{
   /** @var UserRepository */
   private $userRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(User::class);
       $this->userRepository = $manager->getRepository(User::class);
   }


    /**
     * @Route("/mon-profil/{data}/desactive", name="mon-profil-fct-desactive")
     */
    public function _invoke($data)
    {
        $user = $this->getUser();
        switch($data){
            case 1: 
                $user->setFctInteret(false);
                break;
            case 2: 
                $user->setFctCourse(false);
                break;
            case 3: 
                $user->setFctCadeau(false);
                break;
            case 4: 
                $user->setFctConnaissance(false);
                break;
            case 5: 
                $user->setFctAppartement(false);
                break;  
            case 6: 
                $user->setFctRDV(false);
                break;        
        }


        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('mon-profil');
    }
}