<?php


namespace App\Controller\Member;

use App\Entity\listeCadeau\Evenement;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\EvenementRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class ManageCadeauController extends AbstractController
{
   /** @var EvenementRepository */
   private $evenementRepository;

   /** @var UserRepository */
   private $userRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(Evenement::class);
       $this->userRepository = $manager->getRepository(User::class);
       $this->evenementRepository = $manager->getRepository(Evenement::class);
   }


    /**
     * @Route("/liste-cadeau/", name="manageListeCadeau")
     */
    public function _invoke()
    {
        $evenements = $this->evenementRepository->findBy(['user' => $this->getUser()->getId()]);

        return $this->render('member/listeCadeau/list.html.twig',
            [
                'mainNavHome'=>true,
                'evenements' => $evenements,
                'title' => "Liste des cadeaux"
            ]
        );

    }
}