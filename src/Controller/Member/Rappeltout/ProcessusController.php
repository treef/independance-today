<?php


namespace App\Controller\Member\Rappeltout;

use App\Entity\Rappeltout\Processus;
use App\Repository\ProcessusRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class ProcessusController extends AbstractController
{
    /** @var ProcessusRepository */
    private $processusRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(Processus::class);
       $this->processusRepository = $manager->getRepository(Processus::class);
   }


    /**
     * @Route("/processus/{processus}", name="processus")
     */
    public function _invoke(Processus $processus)
    {
        return $this->render('member/Rappeltout/processus.html.twig',
            [
                'mainNavHome'=>true,
                'processus' => $processus,
                //'title' =>  $processus->getTitre()
            ]
        );
    }
}