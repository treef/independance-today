<?php


namespace App\Controller\Member\Rappeltout;

use App\Entity\Rappeltout\Processus;
use App\Repository\ProcessusRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class ListProcessusController extends AbstractController
{
    /** @var ProcessusRepository */
    private $processusRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(Processus::class);
       $this->processusRepository = $manager->getRepository(Processus::class);
   }


    /**
     * @Route("/processus", name="listeprocessus")
     */
    public function _invoke()
    {
        $procs = $this->processusRepository->findBy(["user" => $this->getUser()->getId()]);
        
        return $this->render('member/Rappeltout/listProcessus.html.twig',
            [
                'mainNavHome'=>true,
                'title' => "Liste des processus / articles",
                'procs' => $procs
            ]
        );

    }
}