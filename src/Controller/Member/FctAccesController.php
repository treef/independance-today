<?php


namespace App\Controller\Member;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class FctAccesController extends AbstractController
{
   /** @var UserRepository */
   private $userRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(User::class);
       $this->userRepository = $manager->getRepository(User::class);
   }


    /**
     * @Route("/mon-profil/{data}", name="mon-profil-fct-active")
     */
    public function _invoke($data)
    {
        $user = $this->getUser();
        switch($data){
            case 1: 
                $user->setFctInteret(true);
                break;
            case 2: 
                $user->setFctCourse(true);
                break;
            case 3: 
                $user->setFctCadeau(true);
                break;
            case 4: 
                $user->setFctConnaissance(true);
                break;
            case 5: 
                $user->setFctAppartement(true);
                break;  
            case 6: 
                $user->setFctRDV(true);
                break;        
        }


        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('mon-profil');
    }
}