<?php


namespace App\Controller\Member;

use App\Entity\listeCadeau\Cadeau;
use App\Entity\listeCadeau\Evenement;
use App\Entity\listeCadeau\Member;
use App\Repository\MemberRepository;
use App\Repository\EvenementRepository;
use App\Repository\CadeauRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class CadeauDetailController extends AbstractController
{
    /** @var EvenementRepository */
    private $evenementRepository;

    /** @var UserRepository */
    private $MemberRepository;

    /** @var CadeauRepository */
    private $cadeauRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(Evenement::class);
       $this->userRepository = $manager->getRepository(User::class);
       $this->evenementRepository = $manager->getRepository(Evenement::class);
       $this->cadeauRepository = $manager->getRepository(Cadeau::class);
   }


    /**
     * @Route("/liste-cadeau/{evenement}", name="manageListeCadeau")
     */
    public function _invoke(Evenement $evenement)
    {
        $cadeaux = $this->cadeauRepository->findBy(["Evenement" => $evenement->getId()]);
        $evenement->setCadeaux = $cadeaux;

        return $this->render('member/listeCadeau/list.html.twig',
            [
                'mainNavHome'=>true,
                '$evenement' => $evenement,
                'title' => "Liste des cadeaux - " . $evenement->getLabel() . " " . $evenement->getAnnee() 
            ]
        );

    }
}