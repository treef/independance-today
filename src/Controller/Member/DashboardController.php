<?php


namespace App\Controller\Member;

use App\Entity\listeCadeau\Cadeau;
use App\Entity\User;
use App\Entity\listeCadeau\Evenement;
use App\Entity\listeCadeau\Member;
use App\Entity\rdv\Rdv;
use App\Entity\Interet\Interet;

use App\Repository\MemberRepository;
use App\Repository\EvenementRepository;
use App\Repository\CadeauRepository;
use App\Repository\RDVRepository;
use App\Repository\InteretRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/member")
*/
class DashboardController extends AbstractController
{
    /** @var EvenementRepository */
    private $evenementRepository;

    /** @var UserRepository */
    private $MemberRepository;

    /** @var CadeauRepository */
    private $cadeauRepository;

    /** @var RDVRepository */
    private $rdvRepository;

    /** @var InteretRepository */
    private $interetRepository;

   public function __construct(ManagerRegistry $registry)
   {
       $manager = $registry->getManagerForClass(Evenement::class);
       $this->userRepository = $manager->getRepository(User::class);
       $this->evenementRepository = $manager->getRepository(Evenement::class);
       $this->cadeauRepository = $manager->getRepository(Cadeau::class);
       $this->rdvRepository = $manager->getRepository(Rdv::class);
       $this->interetRepository = $manager->getRepository(Interet::class);
   }


    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function _invoke()
    {
        //var_dump($this->getUser()->getId());
        //die;
        $rdvs = $this->rdvRepository->findAllRdvRecent($this->getUser()->getId());
        $interets = $this->interetRepository->findBy(['user' => $this->getUser()->getId()]);

        //var_dump($rdvs);
        //die;
        return $this->render('Member/dashboard.html.twig',
            [
                'mainNavHome'=>true,
                "user" => $this->getUser(),
                "rdvs" => $rdvs,
                "interets" => $interets, 
                'title' => "Votre Dashboard" 
            ]
        );

    }
}