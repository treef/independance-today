<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ChangeLocaleController extends AbstractController
{
    /**
     * @Route("/change_locale/{locale}", name="change_locale")
     */
    public function changeLocale($locale, Request $request)
    {
        // On stocke la langue dans la session
        $request->getSession()->set("_locale", $locale);
        //$currentRoute = $request->attributes->get('_route');
        //$currentParamsRoute = $request->attributes->get('_route_params');

        // On revient sur la page précédente
        return $this->redirectToRoute("home");
    }
}