<?php

namespace App\Controller\Security;

use App\Entity\Setting;
use App\Form\UserType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
* @Route("/securite")
*/
class RegistrationController extends AbstractController {

    /** @var UserRepository */
    private $userRepository;

    public function __construct(ManagerRegistry $registry)
    {
        $manager = $registry->getManagerForClass(User::class);
        $this->userRepository = $manager->getRepository(User::class);
    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        // 2) handle the submit (will only happen on POST)
        $flag = "";
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            //on active par défaut
            if($this->nb_user() == 0){
                $user->addRole("ROLE_ADMIN");
                $flag = " avec le ROLE ADMIN";
                $user->setAdmin(true);
            }
            else {
                $user->setAdmin(false);
            }

            $setting = new Setting($user);

            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($setting);
            $entityManager->flush();


            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash('success', 'Votre compte à bien été enregistré.'. $flag);
            //return $this->redirectToRoute('login');
        }
        return $this->render('Security/Register/form.html.twig', ['form' => $form->createView(), 'mainNavRegistration' => true, 'title' => 'Inscription']);
    }

    public function nb_user(){
        $sql = $this->userRepository->createQueryBuilder('u')
            ->select('count(u.id)');
        $count_user= $sql
            ->getQuery()->getSingleScalarResult();

        return $count_user;
    }
}
