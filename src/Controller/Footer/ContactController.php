<?php


namespace App\Controller\Footer;

use App\Entity\Contact;
use App\Form\ContactType;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/footer")
*/
class ContactController extends AbstractController
{
    private $logger;

    private $sentdTo;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->sentdTo = "cornado.thibaut@gmail.com";
    }

    /**
     * @Route("/contactez-nous", name="contact_us")
     */
    public function __invoke(\Swift_Mailer $mailer, Request $request)
    {
        // 1) build the form
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setSend("cornado.thibaut@gmail.com");
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            //$this->addFlash('success', 'Votre email a été envoyé.');

            $data = $form->getData();
            $body = $data->getBody();
            $subject = $data->getObject();
            $email = $data->getEmail();

            $this->createEmail($mailer, $email, $body, $subject);

            return $this->redirectToRoute("home");
        }
        return $this->render('Footer/contact.html.twig', [
            'form' => $form->createView(),
            'mainNavRegistration' => true,
            ]);
    }

    public function createEmail(\Swift_Mailer $mailer, $email, $body, $object){
        $message = (new \Swift_Message($object))
            ->setFrom($email)
            ->setTo('cornado.thibaut@gmail.com')
            ->setBody($body)
        ;
        $mailer->send($message);
    }
}