<?php


namespace App\Controller\Footer;


use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/footer")
*/
class aboutController extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/a_propos_de_nous", name="about")
     */
    public function index()
    {
        return $this->render('Footer/about.html.twig');
    }
}