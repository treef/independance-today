<?php


namespace App\Controller\Footer;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/footer")
*/
class legalNoticeController extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/legal_notice", name="legal_notice")
     */
    public function index()
    {
        return $this->render('Footer/about.html.twig');
    }
}