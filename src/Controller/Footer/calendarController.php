<?php


namespace App\Controller\Footer;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/footer")
*/
class calendarController extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/calendrier", name="calendar")
     */
    public function index()
    {
        return $this->render('Footer/calendar.html.twig');
    }
}