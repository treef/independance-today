<?php


namespace App\Controller;

use App\Entity\rdv\Rdv;
use App\Repository\RDVRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;


class IndexController extends AbstractController
{
    private $logger;
    /** @var RDVRepository */
    private $rdvRepository;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

    }

    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        //var_dump($this->get('session')->get('_locale'));

        /*
        $datas = file_get_contents("https://pokeapi.co/api/v2/pokemon/1");
        $json = json_decode($datas, true);
        $abilities = $json->abilities;
        foreach($abilities as $table)
        {
            $hidden = $table->is_hidden;
            $ability = $table->ability;
            $name = $ability->name;
            $url = $ability->url;
            $slot = $table->slot;
            echo "</br>";
            var_dump("nom : $name, url : $url, hidden: $hidden, slot : $slot \n");
        }

        $stats = $json->stats;
        foreach($stats as $stat){
            $base_stat = $stat->base_stat;
            $stat_name = $stat->stat->name;
            echo "base : $base_stat $stat_name ";
        }
        //var_dump($json->abilities);
        die;
        */

        $user = $this->getUser();
        if($user != null)
        {
            /*select * from rdv where rdv.heuredebut <= CURRENT_DATE +7 (mysql)*/
            return $this->RedirectToRoute("dashboard");
        }
        else
        {
            return $this->render('Anonymous/home.html.twig');
        }
        
    }
}