<?php


namespace App\Repository;

use App\Entity\Frigo\Yuka;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Yuka|null find($id, $lockMode = null, $lockVersion = null)
 * @method Yuka|null findOneBy(array $criteria, array $orderBy = null)
 * @method Yuka[]    findAll()
 * @method Yuka[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class YukaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Yuka::class);
    }
}