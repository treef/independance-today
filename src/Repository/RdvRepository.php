<?php


namespace App\Repository;

use App\Entity\rdv\Rdv;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * @method Rdv|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rdv|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rdv[]    findAll()
 * @method Rdv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RdvRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rdv::class);
    }

    public function findAllRdvRecent($id): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT r.id as rdv_id, 
            r.label as rdv_label, 
            r.description as rdv_description, 
            r.heuredebut rdv_heuredebut, 
            r.lieu as rdv_lieu, 
            d.label as destinataire_label, 
            d.lieu as destinataire_lieu 
            from rdv r join destinataire d on r.destinataire = d.id
            WHERE r.user= $id and r.heuredebut >= CURRENT_DATE and r.heuredebut <= CURRENT_DATE + INTERVAL 8 DAY 
            order by r.heuredebut limit 5
            ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAllAssociative();
    }

    public function findAllRdvDisponible($id): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT r.id as rdv_id, 
            r.label as rdv_label, 
            r.description as rdv_description, 
            r.heuredebut rdv_heureDebut, 
            r.heurefin rdv_heureFin, 
            r.lieu as rdv_lieu, 
            d.label as destinataire_label, 
            d.lieu as destinataire_lieu 
            from rdv r join destinataire d on r.destinataire = d.id
            WHERE r.user= $id and r.heuredebut >= CURRENT_DATE + INTERVAL 8 DAY 
            order by r.heuredebut 
            ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAllAssociative();
    }

    public function findAllRdvDestinataireCourant($id, $destinataire_label){
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT r.id as rdv_id, 
            r.label as rdv_label, 
            r.description as rdv_description, 
            r.heuredebut rdv_heureDebut, 
            r.heurefin rdv_heureFin, 
            r.lieu as rdv_lieu, 
            d.label as destinataire_label, 
            d.lieu as destinataire_lieu 
            from rdv r join destinataire d on r.destinataire = d.id
            WHERE r.user= $id and r.heuredebut >= CURRENT_DATE + INTERVAL 8 DAY and d.label = '$destinataire_label'
            order by r.heuredebut 
            ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAllAssociative(); 
    }
}