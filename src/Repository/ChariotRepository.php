<?php


namespace App\Repository;

use App\Entity\Frigo\Chariot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Chariot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chariot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chariot[]    findAll()
 * @method Chariot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChariotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chariot::class);
    }
}