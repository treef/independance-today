<?php


namespace App\Repository;

use App\Entity\Interet\Interet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Interet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Interet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Interet[]    findAll()
 * @method Interet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InteretRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Interet::class);
    }
}