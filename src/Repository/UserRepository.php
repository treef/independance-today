<?php


namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getNb() {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getNbCommande(User $user){
            $conn = $this->getEntityManager()->getConnection();

            $sql = '
            select count(command.id) as nbCommand
            from user 
            inner join command
            ON user.id = command.user
            where user.id = ' . $user->getId();

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $nb = $stmt->fetchAll(Query::HYDRATE_ARRAY)[0]["nbCommand"];
            return $nb;
    }
}