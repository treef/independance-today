<?php


namespace App\Repository;


interface IRoles
{
    const
        ROLE_ADMIN = "ROLE_ADMIN",
        ROLE_MEMBER = "ROLE_MEMBER",
        ROLE_STOCK = "ROLE_STOCK";
}