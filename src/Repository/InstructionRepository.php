<?php


namespace App\Repository;

use App\Entity\Rappeltout\Instruction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Instruction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Instruction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Instruction[]    findAll()
 * @method Instruction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstructionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Instruction::class);
    }
}