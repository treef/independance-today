<?php


namespace App\Repository;

use App\Entity\Frigo\Frigo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Frigo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Frigo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Frigo[]    findAll()
 * @method Frigo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FrigoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Frigo::class);
    }
}