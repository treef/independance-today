<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\rdv\Destinataire;
use App\Entity\User;
use App\Entity\rdv\Rdv;
use App\Repository\IRoles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use DateTime;

class rdvFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
    /** @var UserRepository */
    private $userRepository;

    /** @var RdvRepository */
    private $rdvRepository;

    public function __construct(ManagerRegistry $registry)
    {
        $manager = $registry->getManagerForClass(Rdv::class);
        $this->rdvRepository = $manager->getRepository(Rdv::class);
        $this->userRepository = $manager->getRepository(User::class);
    }

    public function load(ObjectManager $manager)
    {
        if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {

            $user = $this->userRepository->findOneBy(["email" => "admin@admin.fr"]);
            if($user != null)
            {
                $dest = new Destinataire();
                $dest->setLabel("Dentiste");
                $dest->setUser($user);
                $dest2 = new Destinataire();
                $dest2->setLabel("Banque");
                $dest2->setUser($user);
                $dest3 = new Destinataire();
                $dest3->setLabel("diétiticien");
                $dest2->setUser($user);
                $dest4 = new Destinataire();
                $dest4->setLabel("Courtier");
                $dest2->setUser($user);
                $manager->persist($dest);
                $manager->persist($dest2);
                $manager->persist($dest3);
                $manager->persist($dest4);
                
                $array = array($dest, $dest2, $dest3, $dest4);
                foreach($array as $destinataire){
                    for($i=0; $i<= 3; $i++){
                        $rdv = new Rdv();
                        $rdv->setLabel("rdv destinataire $i test du rdv");
                        $rdv->setDescription("");
                        $rdv->setDestinataire($destinataire);
                        $date = $this->randomDateInRange();
                        $randTimeRDV = rand(1,2);
                        $rdv->setHeuredebut(new DateTime($date));
                        $rdv->setHeurefin(new DateTime($date."+$randTimeRDV hours"));
                        $rdv->setLieu("10 impasse des genets 69780 saint pierre de chandieu");
                        $rdv->setUser($user);
                        $manager->persist($rdv);
                    }
                }
            
                $manager->flush();
            }
            else 
            {
                echo "user est null";
            }
        }
    }

    // Find a randomDate between $start_date and $end_date
    function randomDateInRange() {
        $randJour= rand(1, 30);
        $randMois = rand(1,12);
        $randAnnee = 2021;
        $randHeure = rand(0, 23);
        $randMinute = rand(0,59);
        $date = $randAnnee . "-" . $randMois ."-" . $randJour . " " . $randHeure . ":" . $randMinute. ":00";
        return $date;
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class
        );    
    }
}