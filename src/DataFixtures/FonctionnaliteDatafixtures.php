<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\Fonctionnalite;
use App\Repository\IRoles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class FonctionnaliteDatafixtures extends Fixture implements DependentFixtureInterface,  IDatafixtures,IRoles
{

        public function __construct(ManagerRegistry $registry)
        {

        }

        public function load(ObjectManager $manager)
        {
            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
                $fonctionnalite1 = new Fonctionnalite("Interet");
                $fonctionnalite2 = new Fonctionnalite("Cadeau");
                $fonctionnalite3 = new Fonctionnalite("Course");
                $fonctionnalite4 = new Fonctionnalite("Connaissance");
                $fonctionnalite5 = new Fonctionnalite("Appartement");
                $manager->persist($fonctionnalite1);
                $manager->persist($fonctionnalite2);
                $manager->persist($fonctionnalite3);
                $manager->persist($fonctionnalite4);
                $manager->persist($fonctionnalite5);
                $manager->flush();
            }
        }

        public function getDependencies()
        {
            return array(
                UserFixtures::class
            );    
        }
}