<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\User;
use App\Entity\Frigo\Produit;
use App\Entity\Frigo\Frigo;
use App\Entity\Frigo\Chariot;
use App\Repository\IRoles;
use App\Repository\FrigoRepository;
use App\Repository\ChariotRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ProduitFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
        /** @var ChariotRepository */
        private $chariotRepository;

        /** @var FrigoRepository */
        private $frigoRepository;

        /** @var UserRepository */
        private $userRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(Frigo::class);
            $this->frigoRepository = $manager->getRepository(Frigo::class);
            $this->chariotRepository = $manager->getRepository(Chariot::class);
            $this->userRepository = $manager->getRepository(User::class);
        }

        public function load(ObjectManager $manager)
        {
            
            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
                $frigo = $this->frigoRepository->findOneBy(["label" => "Frigo numero 1"]);
                $user = $this->userRepository->findOneBy([
                    "lastname" => "lastname",
                    "firstname" => "name"
                ]);
                $chariots = $this->chariotRepository->findBy(["user"=> $user->getId(), "dedans" => 1]);

                foreach($chariots as $chariot)
                {
                    $produit = new Produit();
                    $produit->setLabel($chariot->getLabel());
                    $produit->setFrigo($frigo);
                    $produit->setChariot($chariot);
                    $manager->persist($produit);
                }

                $manager->flush();
            }
        }

        public function getDependencies()
        {
            return array(
                ChariotFixtures::class,
                FrigoFixtures::class,
                UserFixtures::class
            );    
        }
}