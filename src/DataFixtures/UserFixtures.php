<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\User;
use App\Entity\Setting;
use App\Repository\IRoles;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class UserFixtures extends Fixture implements IDatafixtures,IRoles
{
    private $encoder;

    /** @var UserRepository */
    private $userRepository;

    public function __construct(UserPasswordEncoderInterface $encoder, ManagerRegistry $registry)
    {
        $this->encoder = $encoder;
        $manager = $registry->getManagerForClass(User::class);
        $this->userRepository = $manager->getRepository(User::class);
    }

    public function load(ObjectManager $manager)
    {
        if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
            $user = new User();
            $user->setEmail("admin@admin.fr");
            $user->setUsername("username");
            $user->setFirstname("name");
            $user->setLastname("lastname");
            $user->addRole(self::ROLE_ADMIN);
            $password = $this->encoder->encodePassword($user, 'Tibo2612');
            $user->setPassword($password);
            $user->setAdmin(true);
            $setting = new Setting($user);

            $manager->persist($user);
            $manager->persist($setting);
            $manager->flush();
            for ($i = 0; $i < self::NUMBER_USER_CREATE; $i++) {
                $user = new User();
                $user->setEmail("user@user$i.fr");
                $user->setUsername("username$i");
                $user->setFirstname("name $i");
                $user->setLastname("lastname $i");
                if ($this->nb_user() == "0") {
                    $user->addRole(self::ROLE_ADMIN);
                    $user->setAdmin(true);
                }
                else{
                    $user->setAdmin(false);
                }
                $password = $this->encoder->encodePassword($user, 'password');
                $user->setPassword($password);
                $setting = new Setting($user);

                $manager->persist($setting);
                $manager->persist($user);
                $manager->flush();
            }
        }
    }

    public function getDependencies()
    {
    }

    public function nb_user(){
        $sql = $this->userRepository->createQueryBuilder('u')
            ->select('count(u.id)');
        $count_user= $sql
            ->getQuery()->getSingleScalarResult();

        return $count_user;
    }
}
