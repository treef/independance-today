<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\User;
use App\Entity\Frigo\Chariot;
use App\Repository\IRoles;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ChariotFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
        /** @var UserRepository */
        private $userRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(User::class);
            $this->userRepository = $manager->getRepository(User::class);
        }

        public function load(ObjectManager $manager)
        {

            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
                $user = $this->userRepository->findOneBy([
                    "lastname" => "lastname",
                    "firstname" => "name"
                ]);

                for($i=0; $i<5; $i++)
                {
                    $chariot = new Chariot();
                    $chariot->setLabel("chariot $i");
                    $chariot->setDedans(false);
                    $chariot->setUser($user);
                    $manager->persist($chariot);
                }
                for($i=5; $i<10; $i++)
                {
                    $chariot = new Chariot();
                    $chariot->setLabel("chariot $i");
                    $chariot->setDedans(true);
                    $chariot->setUser($user);
                    $manager->persist($chariot);
                }
                $chariot->setUser($user);
                $manager->persist($chariot);
                $manager->flush();
            }
        }

        public function getDependencies()
        {
            return array(
                UserFixtures::class
            );    
        }
}