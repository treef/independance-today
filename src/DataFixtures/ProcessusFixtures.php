<?php

namespace App\DataFixtures;


use App\Entity\Rappeltout\Part;
use App\Entity\Rappeltout\Processus;
use App\Entity\Rappeltout\Instruction;
use App\Entity\Interet\Categorie;
use App\Entity\User;

use App\Repository\IRoles;
use App\Repository\ProcessusRepository;
use App\Repository\PartRepository;
use App\Repository\InstructionRepository;
use App\Repository\UserRepository;

use App\DataFixtures\Interfass\IDatafixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ProcessusFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
        /** @var ProcessusRepository */
        private $processusRepository;

        /** @var PartRepository */
        private $partRepository;

        /** @var InstructionRepository */
        private $instructionRepository;

        /** @var UserRepository */
        private $userRepository;

        /** @var CategorieRepository */
        private $categorieRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(Processus::class);
            $this->processusRepository = $manager->getRepository(Processus::class);
            $this->partRepository = $manager->getRepository(Part::class);
            $this->instructionRepository = $manager->getRepository(Instruction::class);
            $this->userRepository = $manager->getRepository(User::class);
            $this->categorieRepository = $manager->getRepository(Categorie::class);
        }

        public function load(ObjectManager $manager)
        {
            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
                $user = $this->userRepository->findOneBy(["email" => "admin@admin.fr"]);
                if($user != null )
                {
                    $categorie1 = $this->categorieRepository->findOneBy(["label" => "Informatique"]);

                    $processus1 = new Processus();
                    $processus1->setTitre("Explication processus");
                    $processus1->setUser($user);
                    $processus1->setCategorie($categorie1);
                    $manager->persist($processus1);

                    $processus2 = new Processus();
                    $processus2->setTitre("Explication fct Appartement");
                    $processus2->setUser($user);
                    $processus2->setCategorie($categorie1);
                    $manager->persist($processus2);

                    $part10 = new Part();
                    $part10->setTitrepart("Ajouter un processus");
                    $part10->setDescription("Un processus est une liste organisé en partie et en tâches comme un article organisé par des titres.");
                    $part10->setUser($user);
                    $part10->setProcessus($processus1);
                    $manager->persist($part10);

                    $part11 = new Part();
                    $part11->setTitrepart("Organisation du processus / article");
                    $part11->setDescription("Une partie sera utilisé pour différencié les partie dépendante de chaque article mais chronologique");
                    $part11->setUser($user);
                    $part11->setProcessus($processus1);
                    $manager->persist($part11);

                    $part12 = new Part();
                    $part12->setTitrepart("Les instructions");
                    $part12->setDescription("Les instructions sont les paragraphes qui seront lié par la partie");
                    $part12->setUser($user);
                    $part12->setProcessus($processus1);
                    $manager->persist($part12);

                    $instruction111 = new Instruction();
                    $instruction111->setDescription("Ajouter une partie permet d'organiser le processus / article");
                    $instruction111->setUser($user);
                    $instruction111->setPart($part11);
                    $manager->persist($instruction111);

                    $instruction112 = new Instruction();
                    $instruction112->setDescription("Une partie pourra contenir plusieurs instructions");
                    $instruction112->setUser($user);
                    $instruction112->setPart($part11);
                    $manager->persist($instruction112);

                    $instruction121 = new Instruction();
                    $instruction121->setDescription("Une instructions est une idée, un paragraphe ou une instruction a suivre par exemple pour une recette");
                    $instruction121->setUser($user);
                    $instruction121->setPart($part12);
                    $manager->persist($instruction121);

                    $instruction122 = new Instruction();
                    $instruction122->setDescription("L'instruction est du texte permettant de donner une information");
                    $instruction122->setUser($user);
                    $instruction122->setPart($part12);
                    $manager->persist($instruction122);

                    $manager->flush();
                }
            }
        }

        public function getDependencies()
        {
            return array(
                UserFixtures::class,
            );    
        }
}