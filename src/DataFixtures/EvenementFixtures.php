<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\listeCadeau\Evenement;
use App\Entity\User;
use App\Repository\IRoles;
use App\Repository\EvenementRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class EvenementFixtures extends Fixture implements DependentFixtureInterface,  IDatafixtures,IRoles
{
        /** @var EvenementRepository */
        private $evenementRepository;

        /** @var UserRepository */
        private $userRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(Evenement::class);
            $this->evenementRepository = $manager->getRepository(Evenement::class);
            $this->userRepository = $manager->getRepository(User::class);
        }

        public function load(ObjectManager $manager)
        {
            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
                $user = $this->userRepository->findOneBy(["username" => "username"]);
                
                
                $event = new Evenement();
                $event->setLabel("NOEL");
                $event->setAnnee("2020");
                $manager->persist($event);
                
                $event->setLabel("ANNIVERSAIRE");
                $event->setAnnee("2020");
                $event->setUser($user);
                $manager->persist($event);
                $manager->flush();
            }
        }

        public function getDependencies()
        {
            return array(
                UserFixtures::class,
            );    
        }
}