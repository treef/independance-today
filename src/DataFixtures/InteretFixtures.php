<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\Interet\Interet;
use App\Entity\Interet\Categorie;
use App\Entity\User;
use App\Repository\IRoles;
use App\Repository\InteretRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class InteretFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
        /** @var InteretRepository */
        private $interetRepository;

        /** @var CategorieRepository */
        private $catRepository;

        /** @var UserRepository */
        private $userRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(Interet::class);
            $this->interetRepository = $manager->getRepository(Interet::class);
            $this->catRepository = $manager->getRepository(Categorie::class);
            $this->userRepository = $manager->getRepository(User::class);
        }

        public function load(ObjectManager $manager)
        {
            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {

                $categorie = new Categorie();
                $categorie->setLabel("Informatique");
                $categorie->setTypeCategorie(1);
                $manager->persist($categorie);
                $manager->flush();

                $categorie2 = new Categorie();
                $categorie2->setLabel("Cuisine");
                $categorie2->setTypeCategorie(1);
                $manager->persist($categorie2);
                $manager->flush();


                $user = $this->userRepository->findOneBy(["email" => "admin@admin.fr"]);
                if($user != null )
                {
                    $interet = new Interet();
                    $interet->setLabel("Intelligence artificielle");
                    $interet->setUrl("https://www.woonoz.com/blog/algorithmes-intelligence-artificielle/");
                    $interet->setUser($user);
                    $interet->setCategorie($categorie);
                    $manager->persist($interet);

                    $interet2 = new Interet();
                    $interet2->setLabel("ROBOTIQUE maker");
                    $interet2->setUrl("https://www.cea.fr/comprendre/Pages/nouvelles-technologies/essentiel-sur-robotique.aspx");
                    $interet2->setUser($user);
                    $interet2->setCategorie($categorie);
                    $manager->persist($interet2);

                    $interet2 = new Interet();
                    $interet2->setLabel("Tartelette chocolat");
                    $interet2->setUrl("https://cuisine.journaldesfemmes.fr/recette/320136-tartelette-au-chocolat");
                    $interet2->setUser($user);
                    $interet2->setCategorie($categorie2);
                    $manager->persist($interet2);

                    $manager->flush();
                }
                
            }
        }

        public function getDependencies()
        {
            return array(
                UserFixtures::class,
            );    
        }
}