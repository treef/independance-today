<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\listeCadeau\Evenement;
use App\Entity\listeCadeau\Cadeau;
use App\Entity\listeCadeau\Member;
use App\Repository\IRoles;
use App\Repository\EvenementRepository;
use App\Repository\MemberRepository;
use App\Repository\CadeauRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Exception;

class CadeauFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
        /** @var EvenementRepository */
        private $evenementRepository;

        /** @var MemberRepository */
        private $membreRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(Evenement::class);
            $this->evenementRepository = $manager->getRepository(Evenement::class);
            $this->membreRepository = $manager->getRepository(Member::class);
        }

        public function load(ObjectManager $manager)
        {
            
            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {

                $events = $this->evenementRepository->findAll();
                $membre = $this->membreRepository->findOneBy([
                    "nom"=>"Cornado",
                    "prenom" => "Thibaut"
                ]);
                
                
                foreach($events as $event){
                    $cadeau = new Cadeau();
                    $cadeau->setEvenement($event);
                    $cadeau->setMember($membre);
                    $cadeau->setLabel("test");
                    $cadeau->setUrl('');
                    $manager->persist($cadeau);
                    $manager->flush();
                }
                
            }
        }

        public function getDependencies()
        {
            return array(
                EvenementFixtures::class,
                MemberFixtures::class
                
            );    
        }
}