<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\User;
use App\Entity\Frigo\Frigo;
use App\Repository\IRoles;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class FrigoFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
        /** @var UserRepository */
        private $userRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(User::class);
            $this->userRepository = $manager->getRepository(User::class);
        }

        public function load(ObjectManager $manager)
        {

            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
                $user = $this->userRepository->findOneBy([
                    "lastname" => "lastname",
                    "firstname" => "name"
                ]);

                
                $frigo = new Frigo();
                $frigo->setLabel("Frigo numero 1");
                $frigo->setContenant(20);
                $frigo->setUser($user);
                $manager->persist($frigo);
                $manager->flush();
            }
        }

        public function getDependencies()
        {
            return array(
                UserFixtures::class
            );    
        }
}