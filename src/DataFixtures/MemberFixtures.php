<?php

namespace App\DataFixtures;

use App\DataFixtures\Interfass\IDatafixtures;
use App\Entity\listeCadeau\Groupe;
use App\Entity\listeCadeau\Member;
use App\Entity\User;

use App\Repository\IRoles;
use App\Repository\GroupeRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MemberFixtures extends Fixture implements DependentFixtureInterface, IDatafixtures,IRoles
{
        /** @var GroupeRepository */
        private $groupeRepository;

        public function __construct(ManagerRegistry $registry)
        {
            $manager = $registry->getManagerForClass(Groupe::class);
            $this->groupeRepository = $manager->getRepository(Groupe::class);
        }

        public function load(ObjectManager $manager)
        {

            if($_SERVER["APP_ENV"] == "dev" || $_SERVER["APP_ENV"] == "test") {
                $groupe = $this->groupeRepository->findOneBy(["label" => "Famille"]);
                $membre = new Member();
                $membre->setNom("Cornado");
                $membre->setPrenom("Thibaut");
                $membre->setGroupe($groupe);
                $manager->persist($membre);
                $manager->flush();
            }
        }

        public function getDependencies()
        {
            return array(
                GroupeFixtures::class,
                UserFixtures::class
            );    
        }
}