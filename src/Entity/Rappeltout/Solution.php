<?php

namespace App\Entity\Rappeltout;

use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="Solution")
 * @ORM\Entity()
 * Solution du probleme
 */
class Solution
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $libelle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @ManyToOne(targetEntity="Probleme")
     * @ORM\JoinColumn(name="Probleme", referencedColumnName="id")
     **/
    private $probleme;

    /**
     * @ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    /**
     * @return mixed
     */
    public function getProbleme()
    {
        return $this->probleme;
    }

    /**
     * @param mixed $probleme
     */
    public function setProbleme($probleme): void
    {
        $this->probleme = $probleme;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire): void
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }    
}
