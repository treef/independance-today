<?php

namespace App\Entity\Rappeltout;

use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="Part")
 * @ORM\Entity()
 * partie du processus
 */
class Part
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $titrepart;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ManyToOne(targetEntity="Processus")
     * @ORM\JoinColumn(name="Processus", referencedColumnName="id")
     **/
    private $processus;

    private $instructions;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitrepart()
    {
        return $this->titrepart;
    }

    /**
     * @param mixed $titrepart
     */
    public function setTitrepart($titrepart): void
    {
        $this->titrepart = $titrepart;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param mixed $solutions
     */
    public function setInstructions($instructions): void
    {
        $this->instructions = $instructions;
    }

    /**
     * @return mixed
     */
    public function getProcessus()
    {
        return $this->processus;
    }

    /**
     * @param mixed $processus
     */
    public function setProcessus($processus): void
    {
        $this->processus = $processus;
    }
}
