<?php


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="fonctionnalite")
 * @ORM\Entity(repositoryClass="App\Repository\FonctionnaliteRepository")
 * //Gere une fonctionnalité
 */
class Fonctionnalite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    //Date de création de la fonctionnalite
    /**
     * @ORM\Column(type="datetime")
     */
    private $mep;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $label;

    public function __construct($label)
    {
        $this->mep = new DateTime('now');
        $this->setLabel($label);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMep()
    {
        return $this->mep;
    }

    /**
     * @param mixed $mep
     */
    public function setMep($mep): void
    {
        $this->mep = $mep;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }
}