<?php


namespace App\Entity\nas;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $nomDocument;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $extension;

    /**
     * @ManyToOne(targetEntity="Library", cascade={"persist"})
     * @ORM\JoinColumn(name="Library", referencedColumnName="id", nullable=true)
     **/
    private $library;

    /**
     * @ManyToOne(targetEntity="App\Entity\Interet\Categorie", cascade={"persist"})
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id", nullable=true)
     **/
    private $categorie;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7)
     */
    private $taille;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $mimeType;
    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->setCreated(new DateTime('now'));
    }

    /**
     * @return mixed
     */
    public function getLibrary()
    {
        return $this->library;
    }

    /**
     * @param mixed $library
     */
    public function setLibrary($library): void
    {
        $this->library = $library;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNomDocument()
    {
        return $this->nomDocument;
    }

    /**
     * @param mixed $nomDocument
     */
    public function setNomDocument($nomDocument): void
    {
        $this->nomDocument = $nomDocument;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension): void
    {
        $this->extension = $extension;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille): void
    {
        $this->taille = $taille;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param mixed $mimeType
     */
    public function setMimeType($mimeType): void
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie): void
    {
        $this->categorie = $categorie;
    }
}