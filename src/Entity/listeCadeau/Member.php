<?php


namespace App\Entity\listeCadeau;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\User;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Member")
 * @ORM\Entity(repositoryClass="App\Repository\MemberRepository")
 * personne a qui ont fait le cadeau
 */
class Member
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

        /**
    * @ORM\Column(type="string", length=50)
    */
    private $nom;

    /**
    * @ORM\Column(type="string", length=50)
    */
    private $prenom;
    
    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id", nullable=true)
     **/
    private $user;

    /**
     * @ManyToOne(targetEntity="Groupe", cascade={"persist"})
     * @ORM\JoinColumn(name="Groupe", referencedColumnName="id", nullable=true)
     **/
    private $groupe;

    private $cadeaux;

        /**
     * @ORM\Column(type="datetime")
     */
    private $creer;

    public function __construct()
    {
        $this->creer = new DateTime('now');
    }

        /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }


    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

        /**
     * @return mixed
     */
    public function getGroupe()
    {
        return $this->groupe;
    }

    /**
     * @param mixed $groupe
     */
    public function setGroupe($groupe): void
    {
        $this->groupe = $groupe;
    }

    /**
     * @return mixed
     */
    public function getCadeaux()
    {
        return $this->cadeaux;
    }

    /**
     * @param mixed $cadeaux
     */
    public function setCadeaux($cadeaux): void
    {
        $this->cadeaux = $cadeaux;
    }
}