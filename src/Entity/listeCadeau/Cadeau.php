<?php


namespace App\Entity\listeCadeau;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Cadeau")
 * @ORM\Entity(repositoryClass="App\Repository\CadeauRepository")
 * Cadeau a faire
 */
class Cadeau
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
    * @ORM\Column(type="string", length=100)
    */
    private $url;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ManyToOne(targetEntity="Evenement", cascade={"persist"})
     * @ORM\JoinColumn(name="Evenement", referencedColumnName="id")
     **/
    private $evenement;

    /**
     * @ManyToOne(targetEntity="Member", cascade={"persist"})
     * @ORM\JoinColumn(name="Member", referencedColumnName="id", nullable=true)
     **/
    private $member;

    
    /**
     * @ManyToOne(targetEntity="App\Entity\Interet\Categorie", cascade={"persist"})
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id", nullable=true)
     **/
    private $categorie;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creer;

    public function __construct()
    {
        $this->creer = new DateTime('now');
    }

        /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @param mixed $url
     */
    public function setMember($member): void
    {
        $this->member = $member;
    }

    /**
     * @return mixed
     */
    public function getEvenement()
    {
        return $this->evenement;
    }

    /**
     * @param mixed $evenement
     */
    public function setEvenement($evenement): void
    {
        $this->evenement = $evenement;
    }

            /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie): void
    {
        $this->categorie = $categorie;
    }
}