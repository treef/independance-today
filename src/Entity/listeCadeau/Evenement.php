<?php


namespace App\Entity\listeCadeau;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\User;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Evenement")
 * @ORM\Entity(repositoryClass="App\Repository\EvenementRepository")
 * Evenement participation
 */
class Evenement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
    * @ORM\Column(type="string", length=4, nullable=true)
    */
    private $annee;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creer;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modif;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    private $members;

    private $cadeaux;

    public function __construct()
    {
        $this->creer = new DateTime('now');
        $this->modif = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @param mixed $annee
     */
    public function setAnnee($annee): void
    {
        $this->annee = $annee;
    }

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param mixed $modif
     */
    public function setModif($modif): void
    {
        $this->modif = $modif;
    }

    /**
     * @return mixed
     */
    public function getModif()
    {
        return $this->modif;
    }

    /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }

    /**
     * @param mixed $members
     */
    public function setMembers($members): void
    {
        $this->members = $members;
    }

    /**
     * @return mixed
     */
    public function getMembers()
    {
        return $this->members;
    }

        /**
     * @param mixed $cadeaux
     */
    public function setCadeaux($cadeaux): void
    {
        $this->cadeaux = $cadeaux;
    }

    /**
     * @return mixed
     */
    public function getCadeaux()
    {
        return $this->cadeaux;
    }
}