<?php

namespace App\Entity;

//Permet d'avoir de centraliser les données inhérente au site
interface IBibliothequeDonne
{
    const
        CATEGORIE_INTERET = 1,
        CATEGORIE_CADEAU = 2,
        CATEGORIE_FRIGO = 3,
        CATEGORIE_NAS = 4,
        CATEGORIE_TODOLIST = 5,
        CATEGORIE_OPTIONS_SITE = 6,
        CATEGORIE_OPTION_USER = 7
    ;
}