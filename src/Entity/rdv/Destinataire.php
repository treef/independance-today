<?php


namespace App\Entity\rdv;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Destinataire")
 * @ORM\Entity(repositoryClass="App\Repository\DestinataireRepository") 
 * Information du destinataire
 */
class Destinataire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

        /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creer;

    /**
     * @ORM\Column(type="text", length=250, nullable=true)
     */
    private $lieu;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    public function __construct()
    {
        $this->creer = new DateTime('now');
    }

        /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }

        /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $lieu
     */
    public function setLieu($lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return mixed
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}