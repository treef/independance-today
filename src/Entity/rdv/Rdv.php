<?php


namespace App\Entity\rdv;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="rdv")
 * @ORM\Entity(repositoryClass="App\Repository\RdvRepository")
 * information concernant le rendez vous 
 */
class Rdv
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
    * @ORM\Column(type="string", length=200, nullable=true)
    */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creer;
    
    /**
    * @ORM\Column(type="datetime", length=200)
    */
    private $heuredebut;

    /**
    * @ORM\Column(type="datetime", length=200)
    */
    private $heurefin;

    /**
     * @ManyToOne(targetEntity="Destinataire", cascade={"persist"})
     * @ORM\JoinColumn(name="destinataire", referencedColumnName="id")
     **/
    private $destinataire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lieu;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    public function __construct()
    {
        $this->creer = new DateTime('now');
    }

    /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }

        /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $heurefin
     */
    public function setHeurefin($heure): void
    {
        $this->heurefin = $heure;
    }

    /**
     * @return mixed
     */
    public function getHeurefin()
    {
        return $this->heurefin;
    }

    /**
     * @param mixed $heuredebut
     */
    public function setHeuredebut($heure): void
    {
        $this->heuredebut = $heure;
    }

    /**
     * @return mixed
     */
    public function getHeuredebut()
    {
        return $this->heuredebut;
    }

    /**
     * @param mixed $destinataire
     */
    public function setDestinataire($destinataire): void
    {
        $this->destinataire = $destinataire;
    }

    /**
     * @return mixed
     */
    public function getDestinataire()
    {
        return $this->destinataire;
    }

    /**
     * @param mixed $lieu
     */
    public function setLieu($lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return mixed
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}