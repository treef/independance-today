<?php


namespace App\Entity\Frigo;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Produit")
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 * Produit course oou dans le frigo
 */
class Produit
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

        /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /** @ORM\Column(type="datetime", nullable=true) */
    private $modify;

    /**
    * @ORM\Column(type="string", length=200, nullable=true)
    */
    private $codebarre;

    /**
    * @ORM\Column(type="string", length=200, nullable=true)
    */
    private $marque;

    /**
     * @ORM\Column(type="decimal", scale=3, nullable=true)
     */
    private $montant;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"}, nullable=true)
     */
    private $important;

    /**
     * @ManyToOne(targetEntity="Chariot", cascade={"persist"})
     * @ORM\JoinColumn(name="Chariot", referencedColumnName="id", nullable=true)
     **/
    private $chariot;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    private $note;

        /**
     * @ORM\Column(type="boolean", options={"default":"0"}, nullable=true)
     */
    private $isYuka;

    private $courses;

    /**
     * @ManyToOne(targetEntity="Frigo", cascade={"persist"})
     * @ORM\JoinColumn(name="Frigo", referencedColumnName="id", nullable=true)
     **/
    private $frigo;

    /**
     * @ManyToOne(targetEntity="App\Entity\Interet\Categorie", cascade={"persist"})
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id", nullable=true)
     **/
    private $categorie;

    /** @ORM\Column(type="datetime", nullable=true) */
    private $datePerim;



    public function __construct()
    {
        $this->created = new DateTime('now');
        $this->modify = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getModify()
    {
        return $this->modify;
    }

    /**
     * @param mixed $modify
     */
    public function setModify($modify): void
    {
        $this->modify = $modify;
    }

            /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCodebarre()
    {
        return $this->codebarre;
    }

    /**
     * @param mixed $codebarre
     */
    public function setCodebarre($codebarre): void
    {
        $this->codebarre = $codebarre;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     */
    public function setMarque($marque): void
    {
        $this->marque = $marque;
    }

    /**
     * @return mixed
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param mixed $Montant
     */
    public function setMontant($montant): void
    {
        $this->montant = $montant;
    }

    /**
     * @return mixed
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * @param mixed $important
     */
    public function setImportant($important): void
    {
        $this->important = $important;
    }

    /**
     * @return mixed
     */
    public function getChariot()
    {
        return $this->chariot;
    }

    /**
     * @param mixed $chariot
     */
    public function setChariot($chariot): void
    {
        $this->chariot = $chariot;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note): void
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * @param mixed $courses
     */
    public function setCourses($courses): void
    {
        $this->courses = $courses;
    }

    /**
     * @return mixed
     */
    public function getFrigo()
    {
        return $this->frigo;
    }

    /**
     * @param mixed $frigo
     */
    public function setFrigo($frigo): void
    {
        $this->frigo = $frigo;
    }

    /**
     * @return mixed
     */
    public function getDatePerim()
    {
        return $this->datePerim;
    }

    /**
     * @param mixed $date
     */
    public function setDatePerim($date): void
    {
        $this->datePerim = $date;
    }

    /**
     * @return mixed
     */
    public function getIsYuka()
    {
        return $this->isYuka;
    }

    /**
     * @param mixed $date
     */
    public function setIsYuka($isYuka): void
    {
        $this->isYuka = $isYuka;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie): void
    {
        $this->categorie = $categorie;
    }

    
}