<?php


namespace App\Entity\Frigo;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Frigo")
 * @ORM\Entity(repositoryClass="App\Repository\FrigoRepository")
 * Frigo actuel
 */
class Frigo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
    * @ORM\Column(type="integer", nullable=true)
    */
    private $contenant;

        /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /** @ORM\Column(type="datetime") */
    private $modify;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id", nullable=true)
     **/
    private $user;

    public function __construct()
    {
        $this->created = new DateTime('now');
        $this->modify = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getModify()
    {
        return $this->modify;
    }

    /**
     * @param mixed $modify
     */
    public function setModify($modify): void
    {
        $this->modify = $modify;
    }

    /**
     * @return mixed
     */
    public function getContenant()
    {
        return $this->contenant;
    }

    /**
     * @param mixed $contenant
     */
    public function setContenant($contenant): void
    {
        $this->contenant = $contenant;
    }

        /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

            /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }
}