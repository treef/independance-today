<?php


namespace App\Entity\Frigo;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Yuka")
 * @ORM\Entity(repositoryClass="App\Repository\YukaRepository")
 * infos yuka (info nutritionnel)
 */
class Yuka
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Produit", cascade={"persist"})
     * @ORM\JoinColumn(name="Produit", referencedColumnName="id")
     **/
    private $produit;

    /**
     * @ORM\Column(type="integer")
     */
    private $proteine;

    /**
     * @ORM\Column(type="integer")
     */
    private $fibre;

    /**
     * @ORM\Column(type="integer")
     */
    private $sucre;

    /**
     * @ORM\Column(type="integer")
     */
    private $graisse;

    /**
     * @ORM\Column(type="integer")
     */
    private $sel;

    /**
     * @ORM\Column(type="integer")
     */
    private $calorie;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /** @ORM\Column(type="datetime") */
    private $modify;

    public function __construct()
    {
        $this->created = new DateTime('now');
        $this->modify = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getModify()
    {
        return $this->modify;
    }

    /**
     * @param mixed $modify
     */
    public function setModify($modify): void
    {
        $this->modify = $modify;
    }

        /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

            /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit): void
    {
        $this->produit = $produit;
    }

    /**
     * @return mixed
     */
    public function getProteine()
    {
        return $this->proteine;
    }

    /**
     * @param mixed $proteine
     */
    public function setProteine($proteine): void
    {
        $this->proteine = $proteine;
    }


    /**
     * @return mixed
     */
    public function getFibre()
    {
        return $this->fibre;
    }

    /**
     * @param mixed $fibre
     */
    public function setFibre($fibre): void
    {
        $this->fibre = $fibre;
    }


    /**
     * @return mixed
     */
    public function getSucre()
    {
        return $this->sucre;
    }

    /**
     * @param mixed $sucre
     */
    public function setSucre($sucre): void
    {
        $this->sucre = $sucre;
    }


    /**
     * @return mixed
     */
    public function getGraisse()
    {
        return $this->graisse;
    }

    /**
     * @param mixed $graisse
     */
    public function setGraisse($graisse): void
    {
        $this->graisse = $graisse;
    }

    /**
     * @return mixed
     */
    public function getSel()
    {
        return $this->sel;
    }

    /**
     * @param mixed $sel
     */
    public function setSel($sel): void
    {
        $this->sel = $sel;
    }

    /**
     * @return mixed
     */
    public function getCalorie()
    {
        return $this->calorie;
    }

    /**
     * @param mixed $calorie
     */
    public function setCalorie($calorie): void
    {
        $this->calorie = $calorie;
    }
}