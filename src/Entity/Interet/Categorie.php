<?php


namespace App\Entity\Interet;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Categorie")
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 * Categorie de l'interet (peut egalement servir a gérer d'autre type)
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

        /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
    * @ORM\Column(type="integer")
    */
    private $typeCategorie;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creer;

    public function __construct()
    {
        $this->creer = new DateTime('now');
        $this->setTypeCategorie(1);
    }

        /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }

        /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $typeCategorie
     */
    public function setTypeCategorie($typeCategorie): void
    {
        $this->typeCategorie = $typeCategorie;
    }

    /**
     * @return mixed
     */
    public function getTypeCategorie()
    {
        return $this->typeCategorie;
    }
}