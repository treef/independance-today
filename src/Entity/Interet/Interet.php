<?php


namespace App\Entity\Interet;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Interet")
 * @ORM\Entity(repositoryClass="App\Repository\InteretRepository")
 * Save de memo des interets 
 */
class Interet
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

        /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
    * @ORM\Column(type="string", length=250, nullable=true)
    */
    private $url;

        /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ManyToOne(targetEntity="Categorie", cascade={"persist"})
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id")
     **/
    private $categorie;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creer;

    /**
     * @ORM\Column(name="is_view", type="boolean")
     */
    private $isView;


    public function __construct()
    {
        $this->creer = new DateTime('now');
        $this->isView = false;
    }

        /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie): void
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

        /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $isView
     */
    public function setIsView($isView): void
    {
        $this->isView = $isView;
    }

    /**
     * @return mixed
     */
    public function getIsView()
    {
        return $this->isView;
    }
}