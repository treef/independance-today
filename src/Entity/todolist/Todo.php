<?php


namespace App\Entity\todolist;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="todo")
 * @ORM\Entity(repositoryClass="App\Repository\TodoRepository")
 * Liste des choses a faire 
 */
class Todo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=200)
    */
    private $label;

    /**
    * @ORM\Column(type="string", length=200, nullable=true)
    */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creer;

    /**
     * @ORM\Column(type="datetime")
     */
    private $avantle;

    /**
     * @ORM\Column(type="boolean")
     */
    private $fait;

    /**
     * @ManyToOne(targetEntity="App\Entity\Interet\Categorie", cascade={"persist"})
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id", nullable=true)
     **/
    private $categorie;

    public function __construct()
    {
        $this->creer = new DateTime('now');
        $this->fait = false;
    }

    /**
     * @param mixed $creer
     */
    public function setCreer($creer): void
    {
        $this->creer = $creer;
    }

    /**
     * @return mixed
     */
    public function getCreer()
    {
        return $this->creer;
    }

        /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $heurefin
     */
    public function setHeurefin($heure): void
    {
        $this->heurefin = $heure;
    }

    /**
     * @return mixed
     */
    public function getHeurefin()
    {
        return $this->heurefin;
    }

    /**
     * @param mixed $heuredebut
     */
    public function setHeuredebut($heure): void
    {
        $this->heuredebut = $heure;
    }

    /**
     * @return mixed
     */
    public function getHeuredebut()
    {
        return $this->heuredebut;
    }

    /**
     * @param mixed $avantle
     */
    public function setAvantle($avantle): void
    {
        $this->avantle = $avantle;
    }

    /**
     * @return mixed
     */
    public function getAvantle()
    {
        return $this->avantle;
    }

    /**
     * @param mixed $fait
     */
    public function setFait($fait): void
    {
        $this->fait = $fait;
    }

    /**
     * @return mixed
     */
    public function getFait()
    {
        return $this->fait;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie): void
    {
        $this->categorie = $categorie;
    }
}