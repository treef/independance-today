<?php


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="Utilise")
 * @ORM\Entity(repositoryClass="App\Repository\UtiliseRepository")
 * Association Fonctionnalite avec l'utilisateur
 */
class Utilise
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    //Date de premiére utilisation du user de la fct
    /**
     * @ORM\Column(type="datetime")
     */
    private $create;

    /**
     * @ManyToOne(targetEntity="Fonctionnalite", cascade={"persist"})
     * @ORM\JoinColumn(name="Fonctionnalite", referencedColumnName="id")
     **/
    private $fonctionnalite;

    /**
     * @ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="User", referencedColumnName="id")
     **/
    private $user;

    public function __construct()
    {
        $this->create = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreate()
    {
        return $this->create;
    }

    /**
     * @param mixed $create
     */
    public function setCreate($create): void
    {
        $this->create = $create;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFonctionnalite()
    {
        return $this->fonctionnalite;
    }

    /**
     * @param mixed $fonctionnalite
     */
    public function setFonctionnalite($fonctionnalite): void
    {
        $this->fonctionnalite = $fonctionnalite;
    }
}