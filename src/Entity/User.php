<?php

namespace App\Entity;

use App\Repository\IRoles;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Annotations\DocLexer;
use DateTime;


/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *     fields={"email", "username"}
 * )
 * Permet d'avoir une cession de connexion au site.
 */
class User implements UserInterface, IRoles
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=150, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /** @ORM\Column(type="datetime") */
    private $modify;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=8)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $password;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    private $nbCommand;

    /**
     * @ORM\Column(type="integer")
     */
    private $archieved;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $admin;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $fctRDV;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $fctInteret;

        /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $fctCadeau;

        /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $fctCourse;

        /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $fctConnaissance;

        /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $fctAppartement;

    public function __construct()
    {
        $this->roles = array(self::ROLE_MEMBER);
        $this->created = new DateTime('now');
        $this->modify = new DateTime('now');
        $this->setArchieved(false);
        $this->fctInteret = false;
        $this->fctCadeau = false;
        $this->fctCourse = false;
        $this->fctConnaissance = false;
        $this->fctAppartement = false;
        $this->fctRDV = false;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getModify()
    {
        return $this->modify;
    }

    /**
     * @param mixed $modify
     */
    public function setModify($modify): void
    {
        $this->modify = $modify;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
    }

    public function addRole($role){
        $this->roles[]= $role;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getNbCommand()
    {
        return $this->nbCommand;
    }

    /**
     * @param mixed $nbCommand
     */
    public function setNbCommand($nbCommand): void
    {
        $this->nbCommand = $nbCommand;
    }

    public function getCompleteName()
    {
        return $this->getFirstname(). " " . $this->getLastname();
    }

        /**
     * @param mixed $nbCommand
     */
    public function setArchieved($archieved): void
    {
        $this->archieved = $archieved;
    }
    
    public function getArchieved()
    {
        return $this->archieved;
    }

    function setAdmin($admin) {
        $this->admin = $admin;
    }

    function getAdmin(){
        return $this->admin;
    }

    /**
     * @param mixed $fctInteret
     */
    public function setFctInteret($interet): void
    {
        $this->fctInteret = $interet;
    }
    
    public function getFctInteret()
    {
        return $this->fctInteret;
    }

    /**
     * @param mixed $fctConnaissance
     */
    public function setFctConnaissance($fctConnaissance): void
    {
        $this->fctConnaissance = $fctConnaissance;
    }
    
    public function getFctConnaissance()
    {
        return $this->fctConnaissance;
    }

    /**
     * @param mixed $fctCourse
     */
    public function setFctCourse($fctCourse): void
    {
        $this->fctCourse = $fctCourse;
    }
    
    public function getFctCourse()
    {
        return $this->fctCourse;
    }

    /**
     * @param mixed $fctCadeau
     */
    public function setFctCadeau($fctCadeau): void
    {
        $this->fctCadeau = $fctCadeau;
    }
    
    public function getFctCadeau()
    {
        return $this->fctCadeau;
    }

    /**
     * @param mixed $fctAppartement
     */
    public function setFctAppartement($fctAppartement): void
    {
        $this->fctAppartement = $fctAppartement;
    }
    
    public function getFctAppartement()
    {
        return $this->fctAppartement;
    }

    /**
     * @param mixed $fctRDV
     */
    public function setFctRDV($fctRDV): void
    {
        $this->fctRDV = $fctRDV;
    }
    
    public function getFctRDV()
    {
        return $this->fctRDV;
    }

    function addRoleAdm(){
        $this->addRole("ROLE_ADMIN");
    }

}