<?php


namespace App\Form;


use App\Entity\Setting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('corporatename', TextType::class, ['label'=>'Raison Social', 'attr'=>['class'=>'']])
            ->add('legalstatus', TextType::class, ['label'=>'Status Légal (SARL, SAS, SA ..)', 'attr'=>['class'=>'']])
            ->add('effective', IntegerType::class, ['label'=>'effectif', 'attr'=>['class'=>'']])
            ->add('activity', TextType::class, ['label'=>'activité', 'attr'=>['class'=>'']])
            ->add('address1', TextType::class, ['label'=>'Adresse', 'attr'=>['class'=>'']])
            ->add('codePostal', TextType::class, ['label'=>'Code Postale ', 'attr'=>['class'=>'']])
            ->add('ville', TextType::class, ['label'=>'Ville', 'attr'=>['class'=>'']])
            ->add('date', DateType::class, ['label'=>'Date création société', 'attr'=>['class'=>'']])
            ->add('email', EmailType::class)
            ->add('twitter', TextType::class, [
                'label'=>'Url twitter',
                'required' => false
            ])
            ->add('instagram', TextType::class, [
                'label'=>'Url instagram',
                'required' => false,
            ])
            ->add('facebook', TextType::class, [
                'label'=>'Url facebook',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, ['label'=>'Envoyer', 'attr'=>['class'=>'btn-primary btn-block']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Setting::class
        ]);
    }
}